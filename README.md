# README #

It`s easy to install and run the app. Simply run ```mvn install``` in the root director and then run the generated ```Labyrinth-1.0.jar``` file.

### What is this repository for? ###
 This app implements two algorithms for searching tree or graph data structures: Depth-first and Breadth-fitst. In the project, algorithms are used to find the way to concrete element in matrix. You can change the start configuration such as:

* matrix size
* matrix rarefaction;
* animation speed;
* used algorithm;
* move method;
* move strategy;
* start and destination position.

It clearly demonstrate how these algorithms work.

### How do I get set up? ###
Project was created using java "1.8.0_101". JavaFX is already included in JRE.
 All configurations and dependencies are defined in pom.xml and maven will do everything for you.
You can install app using maven command ```mvn install```

### Who do I talk to? ###
If you have any question - text me rwdiachuk@gmail.com