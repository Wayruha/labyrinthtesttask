package com.sigma;

import com.sigma.controllers.MainController;
import com.sigma.utils.GridCellPane;
import com.sigma.utils.Point;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

public class GridController {

    GridPane mainGrid;
    GridCellPane[][] paneMap;
    Point startPoint, destPoint;
    boolean[][] boolMap;

    public GridController(GridPane mainGrid, Point startPoint, Point destPoint) {
        this.mainGrid = mainGrid;
        this.startPoint = startPoint;
        this.destPoint = destPoint;
    }

    public void createGrid(boolean[][] boolMap) {
        this.boolMap = boolMap;
        int size = boolMap.length;

        mainGrid.getChildren().clear();
        mainGrid.getRowConstraints().clear();
        mainGrid.getColumnConstraints().clear();
        paneMap = new GridCellPane[size][size];

        //add needed count of rows and cols
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(100d / size);
        RowConstraints row = new RowConstraints();
        row.setPercentHeight(100d / size);
        for (int i = 0; i < size; i++) {
            mainGrid.getRowConstraints().add(row);
            mainGrid.getColumnConstraints().add(column);
        }

        //create and fill every cell up
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                boolean isStartPoint = startPoint.getX() == j && startPoint.getY() == i;
                boolean isDestPoint = destPoint.getX() == j && destPoint.getY() == i;
                GridCellPane pane = new GridCellPane(new Point(j, i), boolMap[i][j], isStartPoint, isDestPoint);
                final int k = i, l = j;
                //click  on the pane will change its value
                pane.setOnMouseClicked(event -> {
                    if (MainController.isStarted()) return;
                    if (pane.isDestPoint() || pane.isStartPoint()) return;
                    pane.changeValue();
                    boolMap[k][l] = !boolMap[k][l];
                });

                paneMap[i][j] = pane;
                mainGrid.add(pane, j, i);
            }
        }
    }

    //if we want to create new labyrinth (boolean map)

    public void resetGrid() {
        for (int i = 0; i < paneMap.length; i++) {
            for (int j = 0; j < paneMap[0].length; j++) {
                paneMap[i][j].resetStyles();

            }
        }
    }

    //set and update screen immediately
    public void setStartPoint(Point _startPoint) {
        boolMap[_startPoint.getY()][_startPoint.getX()] = false;
        GridCellPane currentStartPane = paneMap[startPoint.getY()][startPoint.getX()];
        currentStartPane.setStartPoint(false);

        GridCellPane newStartPane = paneMap[_startPoint.getY()][_startPoint.getX()];
        newStartPane.setStartPoint(true);
        this.startPoint = _startPoint;

    }

    public void setDestPoint(Point _destPoint) {
        boolMap[_destPoint.getY()][_destPoint.getX()] = false;
        GridCellPane currentStartPane = paneMap[destPoint.getY()][destPoint.getX()];
        currentStartPane.setDestPoint(false);

        GridCellPane newStartPane = paneMap[_destPoint.getY()][_destPoint.getX()];
        newStartPane.setDestPoint(true);
        this.destPoint = _destPoint;
    }

    public GridCellPane[][] getPaneMap() {
        return paneMap;
    }
}
