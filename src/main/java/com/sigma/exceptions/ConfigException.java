package com.sigma.exceptions;

public class ConfigException extends Exception {
    private String message;

    public ConfigException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
