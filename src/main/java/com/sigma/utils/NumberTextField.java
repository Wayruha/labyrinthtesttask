package com.sigma.utils;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;


public class NumberTextField extends TextField {

    private int minValue, maxValue;
    private Label errorLabel;

    public int getValue() throws NumberFormatException {
        return Integer.parseInt(getText());

    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (text.matches("[0-9]") || text.isEmpty())
            // if (getValue() >= minValue && getValue() <= maxValue || (minValue==0 && maxValue==0))
            super.replaceText(start, end, text);
    }

    @Override
    public void replaceSelection(String replacement) {
        super.replaceSelection(replacement);
    }

    public Label getErrorLabel() {
        return errorLabel;
    }

    public void setErrorLabel(Label errorLabel) {
        this.errorLabel = errorLabel;
        errorLabel.setText("[" + getMinValue() + "-" + getMaxValue() + "]");
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }


}
