package com.sigma.utils;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;


public class GridCellPane extends Pane {

    Point positionInGrid;
    Label label;
    boolean value;
    boolean isStartPoint, isDestPoint;
    Color color;

    public GridCellPane(Point positionInGrid, boolean value, boolean isStartPoint, boolean isDestPoint) {
        this.positionInGrid = positionInGrid;
        this.value = value;
        this.isStartPoint = isStartPoint;
        this.isDestPoint = isDestPoint;
        label = new Label();
        init();

        setStyle("-fx-border-color: black");
    }

    public void init() {
        reset();
        this.getChildren().add(0, label);
        updateColors();

    }

    public void changeValue() {
        //in case of clicking on the cell
        value = !value;
        reset();
    }

    //make it look like a current point
    public void activatePoint() {
        setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public void deactivatePoint() {
        setBackground(new Background(new BackgroundFill(Color.GREENYELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public void resetStyles() {
        reset();
        // updateColors();
    }

    public void setStartPoint(boolean startPoint) {
        isStartPoint = startPoint;
        reset();
    }
    public void setDestPoint(boolean destPoint) {
        isDestPoint = destPoint;
        reset();
    }

    private void reset() {
        if (value) color = Color.BLACK;
        else color = Color.WHITE;

        if (isStartPoint) color = Color.GREEN;
        else if (isDestPoint) color = Color.BLUE;
        updateColors();
    }

    private void updateColors() {
        label.setText(value == true ? "1" : "0");
        label.setTextFill(value == true ? Color.WHITE : Color.BLACK); //font color
        setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
    }



    public boolean isStartPoint() {
        return isStartPoint;
    }



    public boolean isDestPoint() {
        return isDestPoint;
    }


}
