package com.sigma.utils;

import com.sigma.utils.Point;

import java.util.Random;

public class MapGenerator {

    Point startPoint,destPoint;
    private final int rarefactionOffset=5;
    private final double multiplier=0.9;


    public MapGenerator() {

    }

    public boolean[][] generateMap(int size, int rarefactionLevel){
        double rarefaction=convertRarefaction(rarefactionLevel);
        //generate  bool map using some rule
        boolean[][] map=new boolean[size][size];
        for (int k = 0; k < size; k++) {
            for (int l = 0; l < size; l++) {
                    map[k][l] = Math.random() < rarefaction/100 ? true : false;
            }
        }
        map[startPoint.getY()][startPoint.getX()]=false;
        map[destPoint.getY()][destPoint.getX()]=false;
        return map;
    }

    //make the rarefaction to be between [5-95]
    private int convertRarefaction(int rarefactionLevel){
        return rarefactionOffset+(int)(multiplier*rarefactionLevel);

    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public void setDestPoint(Point destPoint) {
        this.destPoint = destPoint;
    }
}
