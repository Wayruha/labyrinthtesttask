package com.sigma.utils;

import com.sigma.AppConfig;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;

import java.util.Collections;

public class Validator {

    public static void notValid(NumberTextField tf) {
        ObservableList<String> styleClass = tf.getStyleClass();
        if (!styleClass.contains("error")) {
            styleClass.add("error");
        }
        if (tf.getErrorLabel() != null) tf.getErrorLabel().setVisible(true);
    }


    public static void becameValid(NumberTextField tf) {
        ObservableList<String> styleClass = tf.getStyleClass();
        styleClass.remove("error");
        if (tf.getErrorLabel() != null) tf.getErrorLabel().setVisible(false);
    }

    public static boolean validateNumberTextField(NumberTextField tf) {
        if ((tf.getMaxValue() == 0 && tf.getMinValue() == 0)) return true; // if limitations weren`t set
        if (tf.getValue() >= tf.getMinValue() && tf.getValue() <= tf.getMaxValue()) {
            becameValid(tf);
            return true;
        }
        notValid(tf);
        return false;
    }

    public static boolean validatePointField(NumberTextField tf) {
        if (!validateNumberTextField(tf)) return false;
        if (tf.getValue() < 0 || tf.getValue() >= AppConfig.getConfig().getSize()) {
            notValid(tf);
            return false;
        }
        else    becameValid(tf);

        return true;

    }
}
