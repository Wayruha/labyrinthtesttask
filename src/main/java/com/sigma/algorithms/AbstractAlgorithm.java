package com.sigma.algorithms;

import com.sigma.Config;
import com.sigma.movePatterns.MoveMethod;
import com.sigma.movePatterns.MoveStrategy;
import com.sigma.utils.Point;

import java.util.ArrayList;

public abstract class AbstractAlgorithm {

    MoveStrategy moveStrategy;
    Config config;
    boolean[][] map;
    Point destPoint, currentPoint;  //they are used in child-classes
    int rows, cols;
    ArrayList<Point> passedPath;
    protected boolean isPathFound;

    public AbstractAlgorithm(boolean[][] map, Config config) {
        this.map = arrayCopy(map);
        rows = map.length;
        cols = map[0].length;
        currentPoint = config.getStartPoint();
        destPoint = config.getDestPoint();
        this.config = config;
        moveStrategy = config.getStrategy();
        passedPath = new ArrayList<>();
    }

    public abstract Point nextPosition();

    public abstract boolean hasNext();

    private boolean[][] arrayCopy(boolean[][] src) {
        if (src != null) {
            final boolean[][] copy = new boolean[src.length][];

            for (int i = 0; i < src.length; i++) {
                final boolean[] row = src[i];
                copy[i] = new boolean[row.length];
                System.arraycopy(row, 0, copy[i], 0, row.length);
            }

            return copy;
        }

        return null;
    }

    protected Point step(Point currentPos, MoveMethod.Direction direction) {
        Point point = new Point(currentPos);
        int newX = point.getX() + direction.getChangeX();
        int newY = point.getY() + direction.getChangeY();
        //if new pos have acceptable  value  - make the changes
        if ((newX >= 0 && newX < cols) && (newY >= 0 && newY < rows))
            point.translate(direction.getChangeX(), direction.getChangeY());
        return point;
    }

    public ArrayList<Point> getPassedPath() {
        return passedPath;
    }

    public boolean isPathFound() {
        return isPathFound;
    }
}
