package com.sigma.algorithms;

import com.sigma.Config;
import com.sigma.utils.Point;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;

import com.sigma.movePatterns.MoveMethod.*;


public class DepthFirstAlgorithm extends AbstractAlgorithm {

    Stack<Point> crossroadStack;
    ArrayList<Direction> directions;
    Direction nextDirection;

    public DepthFirstAlgorithm(boolean[][] map, Config config) {
        super(map, config);
        crossroadStack = new Stack<>();
    }


    @Override
    public Point nextPosition() {
        map[currentPoint.getY()][currentPoint.getX()] = true;

        //find possible ways
        nextDirection = explorePoint(currentPoint);

        if (nextDirection == null)
            currentPoint = popFromCrossroads();
        else
            //if it`s "blind" point - go to the last crossroad
            currentPoint = step(currentPoint, nextDirection);
        passedPath.add(currentPoint);

        if (currentPoint.equals(destPoint)) {
            isPathFound = true;
        }
        return currentPoint;
    }

    @Override
    public boolean hasNext() {
        if (isPathFound) return false;
        if (explorePoint(currentPoint) == null) {
            //pop from stack and then push back if itsn`t not null
            Point point = popFromCrossroads();
            if (point != null) crossroadStack.push(point);
            else
                return false;
        }
        return true;
    }

    private Direction explorePoint(Point point) {
        int countOfPossibleDirections = 0;
        directions = config.getMoveMethod().getDirections();
        config.getStrategy().reorganize(directions); //change the priority in according with Strategy(Clockwise,counterclockwise..)

        Direction nextDirection = null;
        for (Direction direction : directions) {
            if (directionIsClear(point, direction)) {
                // we need to use direction according priority, so we use first-found direction
                if (countOfPossibleDirections == 0) nextDirection = direction;
                countOfPossibleDirections++;
            }
        }
        if (countOfPossibleDirections > 1) if (!crossroadStack.contains(currentPoint)) crossroadStack.add(currentPoint);
        return nextDirection;
    }

    private boolean directionIsClear(Point point, Direction direction) {
        int offsetX = direction.getChangeX();
        int offsetY = direction.getChangeY();
        try {
            return !map[point.getY() + offsetY][point.getX() + offsetX];
        } catch (ArrayIndexOutOfBoundsException ex) {
            return false;
        }
    }

    private Point popFromCrossroads() {
        try {
            Point point = crossroadStack.pop();
            while (explorePoint(point) == null) {
                point = crossroadStack.pop();
            }
            return point;
        } catch (EmptyStackException ex) {
            return null;
        }
    }

}
