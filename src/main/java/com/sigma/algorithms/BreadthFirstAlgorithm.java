package com.sigma.algorithms;


import com.sigma.Config;
import com.sigma.utils.Point;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import com.sigma.movePatterns.MoveMethod.*;

public class BreadthFirstAlgorithm extends AbstractAlgorithm {

    Queue<Point> queue;
    ArrayList<Direction> directions;
    //new part


    public BreadthFirstAlgorithm(boolean[][] map, Config config) {
        super(map, config);
        queue = new LinkedList<>();
    }

    @Override
    public Point nextPosition() {
        map[currentPoint.getY()][currentPoint.getX()] = true;

        directions = config.getMoveMethod().getDirections();
        config.getStrategy().reorganize(directions); //change the priority in according with Strategy(Clockwise,counterclockwise..)

        for (Direction direction : directions) {
            if (directionIsClear(direction)) {
                Point possiblePoint = step(currentPoint, direction);
                if (possiblePoint != null && !queue.contains(possiblePoint)) queue.add(possiblePoint);
            }
        }
        currentPoint = queue.poll();
        passedPath.add(currentPoint);

        if (currentPoint.equals(destPoint)) {
            isPathFound = true;
        }
        return currentPoint;
    }

    @Override
    public boolean hasNext() {
        if (isPathFound) return false;
        //is there any clear direction?
        for (Direction direction : config.getMoveMethod().getDirections())
            if (directionIsClear(direction))
                return true;
        if(queue.isEmpty()) return false;
        return true;
    }


    private boolean directionIsClear(Direction direction) {
        int offsetX = direction.getChangeX();
        int offsetY = direction.getChangeY();
        try {
            return !map[currentPoint.getY() + offsetY][currentPoint.getX() + offsetX];
        } catch (ArrayIndexOutOfBoundsException ex) {
            return false;
        }
    }


}
