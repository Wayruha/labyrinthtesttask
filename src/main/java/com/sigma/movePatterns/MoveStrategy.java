package com.sigma.movePatterns;

import java.util.Collections;
import java.util.List;

//This enum will change the order (priority) of directions ( got from MoveMethod )
public enum MoveStrategy {
    CLOCKWISE("Clockwise starting from top",
            list -> {
            }),

    COUNTERCLOCKWISE("Counterclockwise starting from top",
            list -> {
                Collections.reverse(list);
                MoveMethod.Direction direction = list.remove(list.size() - 1);
                list.add(0, direction);
            }),

    RANDOM("Randomly",
            list -> Collections.shuffle(list));

    private String name;
    private PriorityChanger actionToDo;

    MoveStrategy(String name, PriorityChanger actionToDo) {
        this.name = name;
        this.actionToDo = actionToDo;
    }

    public void reorganize(List<MoveMethod.Direction> list) {
        actionToDo.change(list);
    }

    @Override
    public String toString() {
        return name;
    }

    @FunctionalInterface
    private interface PriorityChanger {
        void change(List<MoveMethod.Direction> list);
    }
}
