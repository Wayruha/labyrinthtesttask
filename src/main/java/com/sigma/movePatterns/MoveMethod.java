package com.sigma.movePatterns;

import java.util.ArrayList;

public enum MoveMethod {
    FOUR_WAYS("4 directions", new int[]{0, 2, 4, 6}),
    EIGHT_WAYS("8 directions", new int[]{0, 1, 2, 3, 4, 5, 6, 7});

    private int[] directionIndexes;
    private String name;

    MoveMethod(String name, int[] directionIndexes) {
        this.name = name;
        this.directionIndexes = directionIndexes;
    }

    public ArrayList<Direction> getDirections() {
        ArrayList<Direction> list = new ArrayList<>();
        for (int index : directionIndexes) {
            list.add(Direction.values()[index]);

        }
        return list;
    }

    @Override
    public String toString() {
        return name;
    }


    public enum Direction {
        TOP(0, -1),
        TOP_RIGHT(1, -1),
        RIGHT(1, 0),
        BOTTOM_RIGHT(1, 1),
        BOTTOM(0, 1),
        BOTTOM_LEFT(-1, 1),
        LEFT(-1, 0),
        TOP_LEFT(-1, -1);

        private int changeX, changeY;

        Direction(int changeX, int changeY) {
            this.changeX = changeX;
            this.changeY = changeY;
        }

        public int getChangeX() {
            return changeX;
        }

        public int getChangeY() {
            return changeY;
        }

    }
}
