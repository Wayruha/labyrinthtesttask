package com.sigma;

import com.sigma.algorithms.AbstractAlgorithm;

import com.sigma.utils.GridCellPane;
import com.sigma.utils.Point;
import javafx.application.Platform;
import javafx.concurrent.Task;


public class RunTask extends Task<Void> {


    GridCellPane[][] paneMap;
    Point startPoint, destPoint;
    boolean isPaused;
    AbstractAlgorithm algorithm;
    int animInterval, minAnimInterval = 900, multiplier = 80, sleepTimeWhenPaused = 400;


    public RunTask(GridCellPane[][] map, Point startPoint, Point destPoint, int animIntervalLevel) throws RuntimeException {
        this.paneMap = map;
        this.startPoint = startPoint;
        this.previousPoint = startPoint.clone();
        this.destPoint = destPoint;
        this.animInterval = minAnimInterval - animIntervalLevel * multiplier;
    }

    @Override
    protected Void call() throws Exception {
        Point nextPoint = startPoint;
        while (algorithm.hasNext()) {
            if (!isPaused) {
                updateMessage("Searching...Checking " + nextPoint + " now");
                nextPoint = algorithm.nextPosition();

                updateCells(nextPoint);
               // previousPoint = nextPoint.clone();
                try {
                    Thread.sleep(animInterval);
                } catch (InterruptedException e) {
                } // it will always interrupt it
            } else {
                // sleep in order not to consume proc time when is paused
                try {
                    Thread.sleep(sleepTimeWhenPaused);
                } catch (InterruptedException e) {
                }
            }


        }
        if (algorithm.isPathFound())
            updateMessage("Destination point was found");
        else
            updateMessage("Couldn`t find the destination point");
        return null;
    }


    public void pause() {
        if (isPaused == false) {
            isPaused = true;
            updateMessage("The search was paused");
        }
    }

    public void resume() {
        if (isPaused == true) isPaused = false;
    }

    public void stop() {
        algorithm = null;
        cancel();
    }

    //it get cancelled only with button "stop"
    @Override
    protected void cancelled() {
        super.cancelled();
        updateMessage("Ready");
    }

    @Override
    protected void failed() {
        super.failed();
        updateMessage("Failed!");
    }

    Point previousPoint;

    private void updateCells(Point newPoint) {
        //Paint next point up like a "head" and current point like one of the "previous" points
        GridCellPane newPane = paneMap[newPoint.getY()][newPoint.getX()];
        GridCellPane previousPane = paneMap[previousPoint.getY()][previousPoint.getX()];
        previousPoint = newPoint;
        Platform.runLater(() -> {
            previousPane.deactivatePoint();
            newPane.activatePoint();
        });
    }

    public void setAlgorithm(AbstractAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public  boolean isPaused() {
        return isPaused;
    }

    public void setMap(GridCellPane[][] map) {
        this.paneMap = map;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public void setDestPoint(Point destPoint) {
        this.destPoint = destPoint;
    }

}