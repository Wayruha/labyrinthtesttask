package com.sigma.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;


public class BottomPaneController {
    static public MainController mainController;
    @FXML
    Button stopButt, playButt;
    @FXML
    AnchorPane statusBar;
    @FXML
    Label statusLabel;

    @FXML
    public void handlePlayButt() {
        if (mainController != null) {
            if (mainController.play()) {
                changeTextInPlayButt();
                statusLabel.textProperty().bind(mainController.runTask.messageProperty());
            }
        }
    }

    @FXML
    public void handleStopButt() {
        if (mainController != null) {
            mainController.stop();
            // resetPlayButt();
        }
    }

    public void resetPlayButt() {
        playButt.setText("Play");
    }

    private void changeTextInPlayButt() {
        if (playButt.getText().toLowerCase().equals("play"))
            playButt.setText("Pause");
        else if (playButt.getText().toLowerCase().equals("pause"))
            playButt.setText("Play");
    }

    public static void setMainController(MainController mainController) {
        BottomPaneController.mainController = mainController;
    }

}
