package com.sigma.controllers;


import com.sigma.AppConfig;
import com.sigma.algorithms.AbstractAlgorithm;
import com.sigma.algorithms.BreadthFirstAlgorithm;
import com.sigma.algorithms.DepthFirstAlgorithm;
import com.sigma.Config;
import com.sigma.exceptions.ConfigException;
import com.sigma.movePatterns.*;
import com.sigma.utils.NumberTextField;
import com.sigma.utils.Point;
import com.sigma.utils.Validator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class ConfigPaneController implements Initializable {

    static MainController mainController;


    @FXML
    NumberTextField matrixSizeTxtFld, rarefactionTxtFld, animationSpeedTxtFld;

    @FXML
    NumberTextField startXTxtFld, startYTxtFld, destXTxtFld, destYTxtFld;

    @FXML
    Label sizeErrorLabel, rarefactionErrorLabel, speedErrorLabel;

    @FXML
    ComboBox<String> algorithmComboBox;
    @FXML
    ComboBox<MoveMethod> moveMethodComboBox;
    @FXML
    ComboBox<MoveStrategy> moveStrategyComboBox;

    Config config;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> algorithmsList = FXCollections.observableArrayList();
        algorithmsList.add("Depth-first search");
        algorithmsList.add("Breadth-first search");

        ObservableList<MoveMethod> moveMethodsList = FXCollections.observableArrayList();
        moveMethodsList.add(MoveMethod.FOUR_WAYS);
        moveMethodsList.add(MoveMethod.EIGHT_WAYS);

        ObservableList<MoveStrategy> movePatternsList = FXCollections.observableArrayList();
        movePatternsList.add(MoveStrategy.CLOCKWISE);
        movePatternsList.add(MoveStrategy.COUNTERCLOCKWISE);
        movePatternsList.add(MoveStrategy.RANDOM);

        algorithmComboBox.setItems(algorithmsList);
        moveMethodComboBox.setItems(moveMethodsList);
        moveStrategyComboBox.setItems(movePatternsList);
        algorithmComboBox.getSelectionModel().select(0);
        moveMethodComboBox.getSelectionModel().select(0);
        moveStrategyComboBox.getSelectionModel().select(0);

        config = AppConfig.getConfig();
        setBindings();

    }


    //Called after start button has been pressed
    public void saveSettings() throws ConfigException {
        if (validate()) {
            int size = matrixSizeTxtFld.getValue();
            int rarefaction = rarefactionTxtFld.getValue();
            int speed = animationSpeedTxtFld.getValue();
            Point startPoint = new Point(startXTxtFld.getValue(), startYTxtFld.getValue());
            Point destPoint = new Point(destXTxtFld.getValue(), destYTxtFld.getValue());
            MoveMethod moveMethod = moveMethodComboBox.getValue();
            MoveStrategy moveStrategy = moveStrategyComboBox.getValue();
            Class<? extends AbstractAlgorithm> algorithmClass;
            switch (algorithmComboBox.getValue()) {
                case "Depth-first search":
                    algorithmClass = DepthFirstAlgorithm.class;
                    break;
                case "Breadth-first search":
                    algorithmClass = BreadthFirstAlgorithm.class;
                    break;
                default:
                    algorithmClass = DepthFirstAlgorithm.class;
                    break;

            }
            //create configuration file
            AppConfig.createConfig(size, rarefaction, speed, startPoint, destPoint, algorithmClass, moveStrategy, moveMethod);
            config = AppConfig.getConfig();
        } else {
            throw new ConfigException("Недопустимі значення налаштувань");
        }

    }

    @FXML
    public void handleGenerateMapBtn() {
        mainController.createGrid();

    }

    //Validate every field. Called when crucial configurations was change (for ex.: size)
    private boolean validate() {
        boolean hasError = false;
        if (!Validator.validateNumberTextField(matrixSizeTxtFld) ||
                !Validator.validateNumberTextField(rarefactionTxtFld) ||
                !Validator.validateNumberTextField(animationSpeedTxtFld) ||

                !Validator.validatePointField(startXTxtFld) |
                        !Validator.validatePointField(startYTxtFld) |
                        !Validator.validatePointField(destXTxtFld) |
                        !Validator.validatePointField(destYTxtFld)
                ) hasError = true;
        return !hasError;  //if there aren`t any errors - return true;
    }

    public static void setMainController(MainController mainController) {
        ConfigPaneController.mainController = mainController;
    }

    //create focusProperty bindings. It`s triggered when the value has changed
    //every textField will validate itself
    private void setBindings() {
        matrixSizeTxtFld.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                int value = matrixSizeTxtFld.getValue();
                if (config.getSize() != value) {
                    config.setSize(value);
                    if (validate())
                        mainController.createGrid();
                }
            }
        });
        rarefactionTxtFld.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                int value = rarefactionTxtFld.getValue();
                if (Validator.validateNumberTextField(rarefactionTxtFld))
                    if (config.getRarefaction() != value) {
                        config.setRarefaction(value);
                        mainController.createGrid();
                    }
            }
        });
        startXTxtFld.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                int value = startXTxtFld.getValue();
                if (Validator.validatePointField(startXTxtFld))
                    if (config.getStartPoint().getX() != value) {
                        Point startPoint = new Point(value, config.getStartPoint().getY());
                        config.setStartPoint(startPoint);
                        mainController.changeStartPoint(startPoint);
                    }
            }
        });
        startYTxtFld.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                int value = startYTxtFld.getValue();
                if (Validator.validatePointField(startYTxtFld))
                    if (config.getStartPoint().getY() != value) {
                        Point startPoint = new Point(config.getStartPoint().getX(), value);
                        config.setStartPoint(startPoint);
                        mainController.changeStartPoint(startPoint);
                    }
            }
        });
        destXTxtFld.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                int value = destXTxtFld.getValue();
                if (Validator.validatePointField(destXTxtFld))
                    if (config.getDestPoint().getX() != value) {
                        Point destPoint = new Point(value, config.getDestPoint().getY());
                        config.setDestPoint(destPoint);
                        mainController.changeDestPoint(destPoint);
                    }
            }
        });
        destYTxtFld.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                int value = destYTxtFld.getValue();
                if (Validator.validatePointField(destYTxtFld))
                    if (config.getDestPoint().getY() != value) {
                        Point destPoint = new Point(config.getDestPoint().getX(), value);
                        config.setDestPoint(destPoint);
                        mainController.changeDestPoint(destPoint);
                    }
            }
        });

    }

}
