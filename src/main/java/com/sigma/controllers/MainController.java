package com.sigma.controllers;

import com.sigma.AppConfig;
import com.sigma.Config;

import com.sigma.GridController;
import com.sigma.exceptions.ConfigException;
import com.sigma.utils.MapGenerator;
import com.sigma.RunTask;
import com.sigma.algorithms.AbstractAlgorithm;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.*;


import com.sigma.utils.Point;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ResourceBundle;


public class MainController implements Initializable {


    @FXML
    GridPane mainGrid;

    boolean[][] boolMap;

    private static boolean isStarted;
    MapGenerator mapGenerator;
    RunTask runTask;
    Thread taskThread;
    Config config;


    @FXML
    private ConfigPaneController configPaneController;
    @FXML
    private BottomPaneController bottomPaneController;

    @FXML
    SplitPane configPaneSplitter;

    GridController gridController;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mapGenerator = new MapGenerator();
        config = AppConfig.getConfig();

        gridController = new GridController(mainGrid, config.getStartPoint(), config.getDestPoint());
        createGrid();

        ConfigPaneController.setMainController(this);
        BottomPaneController.setMainController(this);
    }

    //method is used in other classes (too) in order to let them interact with grid
    public void createGrid() {
        try {
            mapGenerator.setStartPoint(config.getStartPoint());
            mapGenerator.setDestPoint(config.getDestPoint());
            boolMap = mapGenerator.generateMap(config.getSize(), config.getRarefaction());
            gridController.createGrid(boolMap);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR, "Некоректні дані", ButtonType.CANCEL);
            alert.showAndWait();
        }
    }


    private void loadConfig() throws ConfigException {
        configPaneController.saveSettings();
        config = AppConfig.getConfig();
    }


    private void initAlgorithmAndRunIt() {
        AbstractAlgorithm algorithm = null;
        try {
            Class[] cArg = new Class[2]; //Our constructor has 2 arguments
            cArg[0] = boolean[][].class; //search constructor using its arguments
            cArg[1] = Config.class;
            algorithm = config.getAlgorithm().getDeclaredConstructor(cArg).newInstance(boolMap, config);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR, "Помилка створення алгоритму", ButtonType.CANCEL);
            alert.showAndWait();
            return;
        }
        runTask = new RunTask(gridController.getPaneMap(), config.getStartPoint(), config.getDestPoint(), config.getSpeed());
        runTask.setAlgorithm(algorithm);
        runTask.setOnSucceeded(event -> {
            System.out.println("Task has finished");
            killRunTask();
            showConfigPanel();

        });
        taskThread = new Thread(runTask);
        taskThread.setDaemon(true);
        taskThread.start();
        isStarted = true;

    }

    public boolean play() {
        if (runTask == null) {
            //not initialized yet?
            try {
                loadConfig();
            } catch (ConfigException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.CANCEL);
                alert.showAndWait();
                e.printStackTrace();
                return false;
            }

            gridController.resetGrid();
            initAlgorithmAndRunIt();
            hideConfigPanel();
        } else if (runTask.isPaused()) resume();
        else pause();
        return true;

    }

    private void resume() {
        runTask.resume();
    }

    private void pause() {
        runTask.pause();
    }

    private void killRunTask() {
        if (runTask != null) {
            runTask.stop();
            runTask = null;
            taskThread.interrupt();
            isStarted = false;
            bottomPaneController.resetPlayButt();
        }
    }

    public void stop() {
        killRunTask();
        gridController.resetGrid();
        showConfigPanel();
    }

    private void hideConfigPanel() {
        configPaneSplitter.setDividerPosition(0, 0);

    }

    //Methods are used in ConfigPaneController in order to let it interact with grid
    public void changeStartPoint(Point point) {
        gridController.setStartPoint(point);
    }

    public void changeDestPoint(Point point) {
        gridController.setDestPoint(point);
    }

    private void showConfigPanel() {
        configPaneSplitter.setDividerPosition(0, 0.33);
    }

    //It should be visible from every GridCellPane, so i made it public static
    public static boolean isStarted() {
        return isStarted;
    }
}
