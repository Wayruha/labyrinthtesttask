package com.sigma;


import com.sigma.algorithms.AbstractAlgorithm;
import com.sigma.movePatterns.MoveMethod;
import com.sigma.movePatterns.MoveStrategy;
import com.sigma.utils.Point;

public class Config {

    int size, rarefaction, speed;
    Point startPoint, destPoint;
    Class<? extends AbstractAlgorithm> algorithm;
    MoveStrategy strategy;
    MoveMethod moveMethod;


    public Config(int size, int rarefaction, int speed, Point startPoint, Point destPoint, Class<? extends AbstractAlgorithm> algorithm, MoveStrategy strategy, MoveMethod moveMethod) {
        this.size = size;
        this.rarefaction = rarefaction;
        this.speed = speed;
        this.startPoint = startPoint;
        this.destPoint = destPoint;
        this.algorithm = algorithm;
        this.strategy = strategy;
        this.moveMethod = moveMethod;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getRarefaction() {
        return rarefaction;
    }

    public void setRarefaction(int rarefaction) {
        this.rarefaction = rarefaction;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Point getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public Point getDestPoint() {
        return destPoint;
    }

    public void setDestPoint(Point destPoint) {
        this.destPoint = destPoint;
    }

    public Class<? extends AbstractAlgorithm> getAlgorithm() {
        return algorithm;
    }

    public MoveStrategy getStrategy() {
        return strategy;
    }

    public MoveMethod getMoveMethod() {
        return moveMethod;
    }

}


