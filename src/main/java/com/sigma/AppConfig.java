package com.sigma;

import com.sigma.algorithms.AbstractAlgorithm;
import com.sigma.algorithms.DepthFirstAlgorithm;
import com.sigma.movePatterns.MoveMethod;
import com.sigma.movePatterns.MoveStrategy;
import com.sigma.utils.Point;

public class AppConfig {

    static Config config;

    private AppConfig() {
        defaultInit();
    }


    public static Config getConfig() {
        return config;
    }

    public static void createConfig(int size, int rarefaction, int speed, Point startPoint, Point destPoint, Class<? extends AbstractAlgorithm> algorithmClass, MoveStrategy moveStrategy, MoveMethod moveMethod) {
        config = new Config(size, rarefaction, speed, startPoint, destPoint, algorithmClass, moveStrategy, moveMethod);
    }

    public static void defaultInit() {
        final int defSize = 10, defRarefactory = 30, defSpeed = 5;
        final Point defStartPoint = new Point(0, 9),
                defDestPoint = new Point(9, 0);
        final Class<? extends AbstractAlgorithm> defAlgorithm = DepthFirstAlgorithm.class;
        final MoveStrategy defStrategy = MoveStrategy.CLOCKWISE;
        final MoveMethod defMoveMethod = MoveMethod.FOUR_WAYS;
        createConfig(defSize, defRarefactory, defSpeed, defStartPoint, defDestPoint, defAlgorithm, defStrategy, defMoveMethod);
    }

}
