package com.sigma.utils;

import com.sigma.AppConfig;

import com.sigma.algorithms.DepthFirstAlgorithm;
import com.sigma.movePatterns.MoveMethod;
import com.sigma.movePatterns.MoveStrategy;
import javafx.application.Application;
import javafx.stage.Stage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

//Testing validating of Text Fields
public class ValidatorTest {
    NumberTextField field, pointField;

    public static class AsNonApp extends Application {
        @Override
        public void start(Stage primaryStage) throws Exception {
            // noop
        }
    }

    @BeforeClass
    public static void initJFX() {
        Thread t = new Thread("JavaFX Init Thread") {
            public void run() {
                Application.launch(AsNonApp.class, new String[0]);
            }
        };
        t.setDaemon(true);
        t.start();
    }

    @Before
    public void setUp() {
        field = new NumberTextField();
        field.setMaxValue(30);
        field.setMinValue(0);

        pointField = new NumberTextField();
        AppConfig.createConfig(10, 30, 5, new Point(0, 0), new Point(1, 1), DepthFirstAlgorithm.class, MoveStrategy.CLOCKWISE, MoveMethod.FOUR_WAYS);


    }

    @Test()
    public void validateNumberTextField() throws Exception {

        try {
            field.setText("wadawd22");
            Assert.assertFalse(Validator.validateNumberTextField(field));
            Assert.fail();
        } catch (NumberFormatException ex) {
        }

        field.setText("-123");
        Assert.assertFalse(Validator.validateNumberTextField(field));

        field.setText("100");
        Assert.assertFalse(Validator.validateNumberTextField(field));

        List<String> styleClass = field.getStyleClass();
        Assert.assertTrue(styleClass.contains("error"));

        field.setText("15");
        Assert.assertTrue(Validator.validateNumberTextField(field));

        styleClass = field.getStyleClass();
        Assert.assertFalse(styleClass.contains("error"));
    }


    @Test
    public void validatePointField() throws Exception {

        pointField.setText("10");
        Assert.assertFalse(Validator.validatePointField(pointField));
        pointField.setText("-5");
        Assert.assertFalse(Validator.validatePointField(pointField));

        List<String> styleClass = pointField.getStyleClass();
        Assert.assertTrue(styleClass.contains("error"));

        pointField.setText("9");
        Assert.assertTrue(Validator.validatePointField(pointField));

        styleClass = pointField.getStyleClass();
        Assert.assertFalse(styleClass.contains("error"));
    }

}