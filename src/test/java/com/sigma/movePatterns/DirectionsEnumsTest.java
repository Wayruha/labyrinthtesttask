package com.sigma.movePatterns;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;


public class DirectionsEnumsTest {


    @Test
    public void getDirectionsFourWay() {
        ArrayList<MoveMethod.Direction> expectedDirections = new ArrayList<>();
        ArrayList<MoveMethod.Direction> directions = new ArrayList<>();
        directions.addAll(MoveMethod.FOUR_WAYS.getDirections());
        MoveStrategy.CLOCKWISE.reorganize(directions);

        expectedDirections.add(MoveMethod.Direction.TOP);
        expectedDirections.add(MoveMethod.Direction.RIGHT);
        expectedDirections.add(MoveMethod.Direction.BOTTOM);
        expectedDirections.add(MoveMethod.Direction.LEFT);


        Assert.assertEquals(expectedDirections, directions);
        directions.clear();
        directions.addAll(MoveMethod.FOUR_WAYS.getDirections());
        MoveStrategy.COUNTERCLOCKWISE.reorganize(directions);

        expectedDirections.clear();
        expectedDirections.add(MoveMethod.Direction.TOP);
        expectedDirections.add(MoveMethod.Direction.LEFT);
        expectedDirections.add(MoveMethod.Direction.BOTTOM);
        expectedDirections.add(MoveMethod.Direction.RIGHT);

        Assert.assertEquals(expectedDirections, directions);
    }

    @Test
    public void getDirectionsEightWay() {
        ArrayList<MoveMethod.Direction> directions = new ArrayList<>();
        ArrayList<MoveMethod.Direction> expectedDirections = new ArrayList<>();

        directions.addAll(MoveMethod.EIGHT_WAYS.getDirections());
        MoveStrategy.CLOCKWISE.reorganize(directions);


        expectedDirections.add(MoveMethod.Direction.TOP);
        expectedDirections.add(MoveMethod.Direction.TOP_RIGHT);
        expectedDirections.add(MoveMethod.Direction.RIGHT);
        expectedDirections.add(MoveMethod.Direction.BOTTOM_RIGHT);
        expectedDirections.add(MoveMethod.Direction.BOTTOM);
        expectedDirections.add(MoveMethod.Direction.BOTTOM_LEFT);
        expectedDirections.add(MoveMethod.Direction.LEFT);
        expectedDirections.add(MoveMethod.Direction.TOP_LEFT);


        Assert.assertEquals(expectedDirections, directions);
        directions.clear();
        expectedDirections.clear();

        directions.addAll(MoveMethod.EIGHT_WAYS.getDirections());
        MoveStrategy.COUNTERCLOCKWISE.reorganize(directions);

        expectedDirections.add(MoveMethod.Direction.TOP);
        expectedDirections.add(MoveMethod.Direction.TOP_LEFT);
        expectedDirections.add(MoveMethod.Direction.LEFT);
        expectedDirections.add(MoveMethod.Direction.BOTTOM_LEFT);
        expectedDirections.add(MoveMethod.Direction.BOTTOM);
        expectedDirections.add(MoveMethod.Direction.BOTTOM_RIGHT);
        expectedDirections.add(MoveMethod.Direction.RIGHT);
        expectedDirections.add(MoveMethod.Direction.TOP_RIGHT);


        Assert.assertEquals(expectedDirections, directions);
    }

}