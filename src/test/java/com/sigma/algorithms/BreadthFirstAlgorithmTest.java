package com.sigma.algorithms;

import com.sigma.Config;
import com.sigma.movePatterns.MoveMethod;
import com.sigma.movePatterns.MoveStrategy;
import com.sigma.utils.Point;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BreadthFirstAlgorithmTest {
    BreadthFirstAlgorithm algorithm;
    Point start, dest;
    List<Point> expectedPath;
    Config config;
    boolean[][] map;

    @Before
    public void setUp() throws Exception {
        map = new boolean[][]{
                {true, true, false, true, false, false, false},                   //1101000
                {false, false, false, false, false, false, false,},               //0000000
                {false, false, false, false, false, true, false},                 //0000010
                {true, false, true, false, true, false, true},                    //1010101
                {true, false, false, false, false, false, true},                  //1000001
                {false, false, true, true, true, true, true},                     //0011111
                {false, false, false, false, false, false, false}                 //0000000
        };
        start = new Point(0, 6);
        dest = new Point(6, 0);
        expectedPath = new ArrayList<>();
    }

    @Test
    public void checkPathForCorrectnessClockwise() throws Exception {
        config = new Config(7, 30, 5, start, dest, BreadthFirstAlgorithm.class, MoveStrategy.CLOCKWISE, MoveMethod.FOUR_WAYS);
        initExpectedPathClockwise();
        algorithm = new BreadthFirstAlgorithm(map, config);

        while (algorithm.hasNext())
            algorithm.nextPosition();

        Assert.assertEquals(expectedPath, algorithm.getPassedPath());
    }

    @Test
    public void checkPathForCorrectnessClockwiseEightDirections() throws Exception {
        config = new Config(7, 30, 5, start, dest, BreadthFirstAlgorithm.class, MoveStrategy.CLOCKWISE, MoveMethod.EIGHT_WAYS);
        initExpectedPathCLockwiseEightDirections();
        algorithm = new BreadthFirstAlgorithm(map, config);

        while (algorithm.hasNext())
            algorithm.nextPosition();
        Assert.assertEquals(expectedPath, algorithm.getPassedPath());
    }


    private void initExpectedPathClockwise() {
        expectedPath.clear();
        expectedPath.add(new Point(0, 5));
        expectedPath.add(new Point(1, 6));
        expectedPath.add(new Point(1, 5));
        expectedPath.add(new Point(2, 6));
        expectedPath.add(new Point(1, 4));
        expectedPath.add(new Point(3, 6));
        expectedPath.add(new Point(1, 3));
        expectedPath.add(new Point(2, 4));
        expectedPath.add(new Point(4, 6));
        expectedPath.add(new Point(1, 2));
        expectedPath.add(new Point(3, 4));
        expectedPath.add(new Point(5, 6));
        expectedPath.add(new Point(1, 1));
        expectedPath.add(new Point(2, 2));
        expectedPath.add(new Point(0, 2));
        expectedPath.add(new Point(3, 3));
        expectedPath.add(new Point(4, 4));
        expectedPath.add(new Point(6, 6));
        expectedPath.add(new Point(2, 1));
        expectedPath.add(new Point(0, 1));
        expectedPath.add(new Point(3, 2));
        expectedPath.add(new Point(5, 4));
        expectedPath.add(new Point(2, 0));
        expectedPath.add(new Point(3, 1));
        expectedPath.add(new Point(4, 2));
        expectedPath.add(new Point(5, 3));
        expectedPath.add(new Point(4, 1));
        expectedPath.add(new Point(4, 0));
        expectedPath.add(new Point(5, 1));
        expectedPath.add(new Point(5, 0));
        expectedPath.add(new Point(6, 1));
        expectedPath.add(new Point(6, 0));
    }

    private void initExpectedPathCLockwiseEightDirections() {
        expectedPath.clear();
        expectedPath.add(new Point(0, 5));
        expectedPath.add(new Point(1, 5));
        expectedPath.add(new Point(1, 6));
        expectedPath.add(new Point(1, 4));
        expectedPath.add(new Point(2, 4));
        expectedPath.add(new Point(2, 6));
        expectedPath.add(new Point(1, 3));
        expectedPath.add(new Point(3, 3));
        expectedPath.add(new Point(3, 4));
        expectedPath.add(new Point(3, 6));
        expectedPath.add(new Point(1, 2));
        expectedPath.add(new Point(2, 2));
        expectedPath.add(new Point(0, 2));
        expectedPath.add(new Point(3, 2));
        expectedPath.add(new Point(4, 2));
        expectedPath.add(new Point(4, 4));
        expectedPath.add(new Point(4, 6));
        expectedPath.add(new Point(1, 1));
        expectedPath.add(new Point(2, 1));
        expectedPath.add(new Point(0, 1));
        expectedPath.add(new Point(3, 1));
        expectedPath.add(new Point(4, 1));
        expectedPath.add(new Point(5, 1));
        expectedPath.add(new Point(5, 3));
        expectedPath.add(new Point(5, 4));
        expectedPath.add(new Point(5, 6));
        expectedPath.add(new Point(2, 0));
        expectedPath.add(new Point(4, 0));
        expectedPath.add(new Point(5, 0));
        expectedPath.add(new Point(6, 0));


    }

}